dateFormatVal = "dd/mm/yy";
jQuery(document).ready(function () {
  jQuery('body.patient_user .remicade_title_grey span.circle_text').html("REMICADE<sup>&reg;</sup> INFUSION &nbsp; DATES");
  function add() {
    if (jQuery(this).val() === '') {
      jQuery(this).val(jQuery(this).attr('placeholder')).addClass('placeholder');
    }
  }

  function remove() {
    if (jQuery(this).val() === jQuery(this).attr('placeholder')) {
      jQuery(this).val('').removeClass('placeholder');
    }
  }

  // Create a dummy element for feature detection
  if (!('placeholder' in jQuery('<input>')[0])) {

    // Select the elements that have a placeholder attribute
    jQuery('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

    // Remove the placeholder text before the form is submitted
    jQuery('form').submit(function () {
      jQuery(this).find('input[placeholder], textarea[placeholder]').each(remove);
    });
  }
  jQuery(".txt-calendar").datepicker({'dateFormat': dateFormatVal});
  jQuery('.calender_icon').click(function () {
    jQuery('.txt-calendar').datepicker('hide');
    calId = '#actual_date_' + jQuery(this).attr('rel');
    jQuery(calId).datepicker('show');
  });

  jQuery('.user-date-input').change(function () {
    checkDate(this, jQuery(this).val());
  });



  jQuery('#patient_is').change(function () {
    if (jQuery(this).val() == 'continue')
    {
      jQuery('.div_continue').slideDown(400);
      jQuery('.lbl_start').html('Date of first REMICADE infusion<br /> on current script<sup>*</sup>');
      jQuery('.lbl_start').addClass('margin');
      jQuery('.lbl_start').addClass('width_label');
      jQuery('#spnWeeksCount').html('40');

      jQuery("#second_infusion, #third_infusion, #fourth_infusion, #fifth_infusion, #sixth_infusion").html("Next infusion");
      jQuery("#third_infusion").html();
      jQuery("#fourth_infusion").html();
      jQuery("#fifth_infusion").html();
      jQuery("#sixth_infusion").html();
    } else {
      jQuery('.div_continue').slideUp(400);
      jQuery('.lbl_start').html('REMICADE start date*');
      jQuery('.lbl_start').removeClass('margin');
      jQuery('.lbl_start').removeClass('width_label');
      jQuery('#spnWeeksCount').html('30');

      jQuery("#second_infusion").html("2nd infusion");
      jQuery("#third_infusion").html("3rd infusion");
      jQuery("#fourth_infusion").html("4th infusion");
      jQuery("#fifth_infusion").html("5th infusion");
      jQuery("#sixth_infusion").html("6th infusion");
    }
    jQuery('#start_date').val('');
    jQuery('.grey_input_field').val('');

  });

  jQuery("#start_date").datepicker({
    'dateFormat': dateFormatVal,
    "onClose": function () {
      if (checkStartDate()) {
        updateInfusionDate(jQuery(this).val());
        jQuery('.txt-calendar').css('border', 'solid 1px red');
      } else {
        jQuery('.grey_input_field').val('');
      }
    }
  });

  jQuery("#start_date").blur(function () {
    checkStartDate();
  });

  jQuery("#start_date").change(function () {
    checkStartDate();
  });

  jQuery("#last_assessment_date").datepicker({
    'dateFormat': dateFormatVal
  });
  jQuery("#last_posting_date").datepicker({
    'dateFormat': dateFormatVal
  });

  jQuery('#start_date_icon').click(function () {
    jQuery('.txt-calendar').datepicker('hide');
    jQuery('#start_date').datepicker('show');
  });
  jQuery('#last_assessment_date_icon').click(function () {
    jQuery('.txt-calendar').datepicker('hide');
    jQuery('#last_assessment_date').datepicker('show');
  });
  jQuery('#last_posting_date_icon').click(function () {
    jQuery('.txt-calendar').datepicker('hide');
    jQuery('#last_posting_date').datepicker('show');
  });

  jQuery('.btn-pdf-submit').click(function () {
    jQuery('#action_type').val('pdf');
    checkStartDate();
    if (jQuery('#start_date').val() != "") {
      jQuery('#hidden-save').click();
    } else {
      if (jQuery('#patient_is').val() == 'start' || jQuery('#patient_is').val() == '') {
        alert('Please complete the REMICADE Start Date highlighted.');
      } else {
        alert('Please complete the treatment history fields highlighted.');
      }
      return false;
    }
  });

  //vali9date

  jQuery(".user-input-txt").keydown(function (e) {
    status = false;
    val = jQuery(this).val();
    if ((e.which == 32 || e.which == 46 || e.which == 9 || e.which == 8 || e.which == 36 || e.which == 35) || (e.which >= 65 && e.which <= 90) || (e.which >= 37 && e.which <= 40)) {
      return  true;
    } else {
      return  false;

    }
  });

  jQuery(".user-input-alphanum").keydown(function (e) {

    status = false;
    val = jQuery(this).val();


    if ((e.which == 32 || e.which == 109 || e.which == 173 || e.which == 8 || e.which == 9 || e.which == 16 || e.which == 36 || e.which == 35 || e.which == 46 || e.which == 189) || (e.which >= 37 && e.which <= 40) || (e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105) || e.key == "-") {
      return  true;
    } else {
      return  false;

    }

  });

  jQuery(".user-input-alphanum").blur(function (e) {

  });

  jQuery(".user-input-alphanum").keyup(function () {
    var $th = jQuery(this);
    $th.val($th.val().replace(/[^0-9-]/g, function (str) {
      if (str == ' ')
        return ' ';
      return '';
    }));
  });
});
dt = new Date();
function updateInfusionDate(init_date) {

  h = 24;
  //[A] init_date;
  y = parseInt(getDatePart(init_date, 'y'));
  m = parseInt(getDatePart(init_date, 'm'));
  d = parseInt(getDatePart(init_date, 'd'));

  dt = new Date(y, m, d);

  if (jQuery('#patient_is').val() != 'continue') {
    // [B] = [A] + (2*7days)
    dt.setHours(+2 * 7 * 24);

    i_date_0 = getDateStr(dt);
    jQuery('#i_date_0').val(i_date_0);


    //[C] =[A]+(6*7days)
    dt = new Date(y, m, d);
    dt.setHours(6 * 7 * h);
    i_date_1 = getDateStr(dt);
    jQuery('#i_date_1').val(i_date_1);

    //[D1] =[A]+(8*7days)
    dt = new Date(y, m, d);
    dt.setHours(8 * 7 * h);
    pbs_from_date_0 = getDateStr(dt);
    jQuery('#pbs_from_date_0').val(pbs_from_date_0);

    //[D2] =[A]+(11*7days)
    dt = new Date(y, m, d);
    dt.setHours(11 * 7 * h);
    pbs_to_date_0 = getDateStr(dt);
    jQuery('#pbs_to_date_0').val(pbs_to_date_0);

    //[E] =[C]+(8*7days)
    y = getDatePart(i_date_1, 'y');
    m = getDatePart(i_date_1, 'm');
    d = getDatePart(i_date_1, 'd');
    dt = new Date(y, m, d);
    dt.setHours(8 * 7 * h);
    i_date_2 = getDateStr(dt);
    jQuery('#i_date_2').val(i_date_2);

    //[F] =[C]+(16*7days)
    dt = new Date(y, m, d);
    dt.setHours(16 * 7 * h);
    i_date_3 = getDateStr(dt);
    jQuery('#i_date_3').val(i_date_3);

    //[G] =[C]+(24*7days)
    dt = new Date(y, m, d);
    dt.setHours(24 * 7 * h);
    i_date_4 = getDateStr(dt);
    jQuery('#i_date_4').val(i_date_4);

    //[H1] =[E]+(18*7days)
    y = getDatePart(i_date_2, 'y');
    m = getDatePart(i_date_2, 'm');
    d = getDatePart(i_date_2, 'd');
    dt = new Date(y, m, d);
    dt.setHours(18 * 7 * h);
    pbs_from_date_1 = getDateStr(dt);
    jQuery('#pbs_from_date_1').val(pbs_from_date_1);

    //[H2] =[E]+(21*7days)
    dt = new Date(y, m, d);
    dt.setHours(21 * 7 * h);
    pbs_to_date_1 = getDateStr(dt);
    jQuery('#pbs_to_date_1').val(pbs_to_date_1);
  } else {

    // [s] = [p] + (8*7days)
    dt.setHours(+8 * 7 * 24);

    i_date_0 = getDateStr(dt);
    jQuery('#i_date_0').val(i_date_0);


    //[T] =[P]+(16*7days)
    dt = new Date(y, m, d);
    dt.setHours(16 * 7 * h);
    i_date_1 = getDateStr(dt);
    jQuery('#i_date_1').val(i_date_1);

    //[U1] =[A]+(18*7days)
    dt = new Date(y, m, d);
    dt.setHours(18 * 7 * h);
    pbs_from_date_0 = getDateStr(dt);
    jQuery('#pbs_from_date_0').val(pbs_from_date_0);

    //[U2] =[A]+(21*7days)
    dt = new Date(y, m, d);
    dt.setHours(21 * 7 * h);
    pbs_to_date_0 = getDateStr(dt);
    jQuery('#pbs_to_date_0').val(pbs_to_date_0);

    //[V] =[T]+(8*7days)
    y = getDatePart(i_date_1, 'y');
    m = getDatePart(i_date_1, 'm');
    d = getDatePart(i_date_1, 'd');
    dt = new Date(y, m, d);
    dt.setHours(8 * 7 * h);
    i_date_2 = getDateStr(dt);
    jQuery('#i_date_2').val(i_date_2);

    //[W] =[T]+(16*7days)
    dt = new Date(y, m, d);
    dt.setHours(16 * 7 * h);
    i_date_3 = getDateStr(dt);
    jQuery('#i_date_3').val(i_date_3);

    //[X] =[T]+(24*7days)
    dt = new Date(y, m, d);
    dt.setHours(24 * 7 * h);
    i_date_4 = getDateStr(dt);
    jQuery('#i_date_4').val(i_date_4);

    //[Y1] =[V]+(18*7days)
    y = getDatePart(i_date_2, 'y');
    m = getDatePart(i_date_2, 'm');
    d = getDatePart(i_date_2, 'd');
    dt = new Date(y, m, d);
    dt.setHours(18 * 7 * h);
    pbs_from_date_1 = getDateStr(dt);
    jQuery('#pbs_from_date_1').val(pbs_from_date_1);

    //[Y2] =[V]+(21*7days)
    dt = new Date(y, m, d);
    dt.setHours(21 * 7 * h);
    pbs_to_date_1 = getDateStr(dt);
    jQuery('#pbs_to_date_1').val(pbs_to_date_1);
  }
}

function checkStartDate() {
  if (jQuery('#start_date').val() == '') {
    jQuery('#start_date').css('border', '1px solid red');
    return false;
  } else {
    jQuery('#start_date').css('border', '0 none');
    return true;
  }
}

function getDateStr(objDate) {
  //console.log(objDate);
  date_str = objDate.getDate() + '/' + (objDate.getMonth() + 1) + '/' + objDate.getFullYear();
  return date_str;
}

//get year or month or day from given date 
function getDatePart(date_str, part_type) {
  dateArr = getDateArr(date_str);
  if (part_type == 'm')
    return dateArr[1] - 1;
  else if (part_type == 'd')
    return dateArr[0];
  else if (part_type == 'y')
    return dateArr[2];

  return date_str;
}

function getDateArr(date_str) {
  date_str = date_str.replace('/', '-');
  date_str = date_str.replace('/', '-');
  date_str = date_str.replace('/', '-');
  dateArr = date_str.split('-');

  return dateArr;
}

function isValidDate(val) {
  var y = parseInt(getDatePart(val, 'y'));
  var m = parseInt(getDatePart(val, 'm'));
  var d = parseInt(getDatePart(val, 'd'));
  var dt = new Date(y, m, d);
  if (dt.toString() == "Invalid Date") {
    return false;
  } else {
    return true;
  }
}

function checkDate(item) {
  val = jQuery(item).val();
  if (!isValidDate(val)) {
    if (jQuery('#ui-datepicker-div').css('display') != 'block') {
      jQuery(item).val('');
      alert('Invalid Date');
      jQuery(item).focus();
    }
  }
}
function PrintElem(elem)
{
  Popup(jQuery(elem).html());
}

function Popup(data)
{
  var mywindow = window.open('', 'INFUSION AND ASSESSMENT SCHEDULE', 'scrollbars=1, width=900');

  mywindow.document.write(data);
  mywindow.document.close();
  mywindow.focus();
  mywindow.print();
  mywindow.close();

  return true;
}

function getUpdateHtml() {
  jQuery('#action_type').val('print');
  checkStartDate();
  if (jQuery('#start_date').val() != "") {
    jQuery.ajax({
      url: window.location.toString(),
      data: jQuery('#frm_calc').serialize(),
      type: 'post'
    }).done(function (r) {
      if (r == 'error') {
        window.location = '/';
      } else {
        Popup(r);
      }

    });
  } else {
    if (jQuery('#patient_is').val() == 'start' || jQuery('#patient_is').val() == '') {
      alert('Please complete the REMICADE Start Date highlighted.');
    } else {
      alert('Please complete the treatment history fields highlighted.');
    }
    return false;
  }
}