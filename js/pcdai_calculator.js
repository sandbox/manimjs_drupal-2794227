jQuery(function () {

  jQuery(".q-rbtn").change(function () {
    var current_value;
    if (jQuery(this).attr("rel") == 9)
    {
      update_question_9_score();
    }
    else if (jQuery(this).attr("rel") == 5)
    {
      update_question_5_score();
    }
    else
    {
      current_value = jQuery(this).val();
      var question_score = jQuery(this).attr("name") + "_score";
      jQuery("#" + question_score).val(current_value);
    }
    update_total_score();
  });

  jQuery('.btn-pdf-submit').click(function () {
    jQuery('#action_type').val('pdf');
    validate_questions();
    if (validate_questions()) {
      if (jQuery('#total_score').val() != "") {
        jQuery('#hidden-save').click();
      } else {
        alert('Please select options for all the questions.');
        return false;
      }
    }
  });

  jQuery("#height_type").change(function () {
    show_relative_height_type();
  });

  show_relative_height_type();

  jQuery("#question_9_hct").keyup(function () {
    update_question_9_score();
  });

  jQuery("input[name=question_9]").on("click", function () {
    //console.log(jQuery(this).val());
    if (jQuery(this).val() == "male")
    {
      show_question_9_male_ages();
    }
    else if (jQuery(this).val() == "female")
    {
      show_question_9_female_ages();
    }
  })
});



function show_question_9_male_ages()
{
  jQuery(".f_only_age").each(function () {
    jQuery(this).hide();
  });
  jQuery(".m_only_age").each(function () {
    jQuery(this).show();
  });
}

function show_question_9_female_ages()
{
  jQuery(".m_only_age").each(function () {
    jQuery(this).hide();
  });
  jQuery(".f_only_age").each(function () {
    jQuery(this).show();
  });
}
function update_question_5_score()
{
  if (jQuery("#height_type").val() == "velocity")
  {
    current_value = jQuery("input[name='question_5_1']:checked").val();
  }
  else {
    current_value = jQuery("input[name='question_5']:checked").val();
  }
  jQuery("#question_5_score").val(current_value);
}
function update_question_9_score()
{
  var sex, age, hct, score = 0;
  sex = jQuery("input[name='question_9']:checked").val();
  age = jQuery("input[name='question_9_age']:checked").val();
  hct = jQuery("#question_9_hct").val();
  hct = (hct ? hct : 0);
  //console.log("Sex: "+sex+"; Age: "+age+"; HCT: "+hct);
  if (sex == "male")
  {
    if (age == "6-10")
    {
      if (hct >= 33)
      {
        score = 0;
      }
      else if (hct >= 28 && hct <= 32)
      {
        score = 2.5;
      }
      else if (hct < 28)
      {
        score = 5;
      }
    }
    else if (age == "11-14")
    {
      if (hct >= 35)
      {
        score = 0;
      }
      else if (hct >= 30 && hct <= 34)
      {
        score = 2.5;
      }
      else if (hct < 30)
      {
        score = 5;
      }
    }
    else if (age == "15-19")
    {
      if (hct >= 37)
      {
        score = 0;
      }
      else if (hct >= 32 && hct <= 36)
      {
        score = 2.5;
      }
      else if (hct < 32)
      {
        score = 5;
      }
    }
  }
  else
    (sex == "female")
  {
    if (age == "6-10")
    {
      if (hct >= 33)
      {
        score = 0;
      }
      else if (hct >= 28 && hct <= 32)
      {
        score = 2.5;
      }
      else if (hct < 28)
      {
        score = 5;
      }
    }
    else if (age == "11-19")
    {
      if (hct >= 34)
      {
        score = 0;
      }
      else if (hct >= 29 && hct <= 33)
      {
        score = 2.5;
      }
      else if (hct < 29)
      {
        score = 5;
      }
    }
  }

  var question_score = "question_9_score";
  jQuery("#" + question_score).val(score);
}

function validate_amount_value(e, obj)
{
  var prev_value = jQuery(obj).val();
  var chCode = ('charCode' in e) ? e.charCode : e.keyCode;
  //alert(chCode);
  //alert(e.keyCode + "\n" + e.which+"\n"+e.charCode);
  //Allow 0 to 9, backspace or delete and dot, left arrow(37) right arrow(39) only.
  //if( ([e.keyCode||e.which] >= 48 && [e.keyCode||e.which] <= 57) || [e.keyCode||e.which] == 8 || [e.keyCode||e.which] == 46 || [e.keyCode||e.which] == 37 || [e.keyCode||e.which] == 39 )
  if ((chCode >= 48 && chCode <= 57) || chCode == 8 || chCode == 46 || chCode == 0 || chCode == 45)
  {
    if ([e.keyCode || e.which] == 46)
    {
      var n = prev_value.indexOf(".");
      if (n == -1)
      {

      }
      else
      {
        jQuery(obj).val(prev_value);
        e.preventDefault ? e.preventDefault() : e.returnValue = false;
      }
    }

    if ([e.keyCode || e.which] == 45)
    {
      var neg = prev_value.indexOf("-");
      if (neg == -1)
      {

      }
      else
      {
        jQuery(obj).val(prev_value);
        e.preventDefault ? e.preventDefault() : e.returnValue = false;
      }
    }
  }
  else
  {
    e.preventDefault ? e.preventDefault() : e.returnValue = false;
  }
}

function show_relative_height_type()
{
  jQuery("input[name=question_5_1]").each(function ()
  {
    jQuery(this).prop('checked', false);
  });
  jQuery("input[name=question_5]").each(function ()
  {
    jQuery(this).prop('checked', false);
  });
  jQuery("#question_5_score").val("");

  if (jQuery("#height_type").val() == "velocity")
  {
    jQuery("#height_type_diagnosis").hide();
    jQuery("#height_type_velocity").show();
  }
  else
  {
    jQuery("#height_type_velocity").hide();
    jQuery("#height_type_diagnosis").show();
  }
}

function validate_questions() {
  var return_flag = true;
  jQuery(".question-score").each(function () {

    var question_index = jQuery(this).attr('rel');

    if (jQuery(this).val() == "") {
      jQuery('#err_' + question_index).html('Please select an answer for this question');
      return_flag = false;
    } else {
      jQuery('#err_' + question_index).html('');
    }
  });

  return return_flag;
}

function update_total_score()
{
  var total_score = 0;
  var val = 0;
  jQuery(".question-score").each(function () {
    val = jQuery(this).val();
    if (val)
    {
      total_score += parseFloat(jQuery(this).val());
    }
  });

  if (total_score || total_score == 0)
  {
    jQuery("#total_score").val(total_score);
  }
  else
  {
    jQuery("#total_score").val("");
  }
}

function getUpdateHtml() {
  jQuery('#action_type').val('print');
  validate_questions();
  if (validate_questions()) {
    jQuery.ajax({
      url: window.location.toString(),
      data: jQuery('#frm_calc').serialize(),
      type: 'post'
    }).done(function (r) {
      if (r == 'error') {
        window.location = '<?php echo get_site_url(); ?>';
      } else {
        Popup(r);
      }
    });
  }
}

function Popup(data)
{
  var mywindow = window.open('', 'PUCAI Calculator', 'scrollbars=1, width=900');

  mywindow.document.write(data);
  mywindow.document.close();
  mywindow.focus();
  mywindow.print();
  mywindow.close();

  return true;
}