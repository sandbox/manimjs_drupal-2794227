function validate_questions() {
  var return_flag = true;
  jQuery(".question-score").each(function () {

    var question_index = jQuery(this).attr('rel');

    if (jQuery(this).val() == "") {
      jQuery('#err_' + question_index).html('Please select an answer for this question');
      return_flag = false;
    } else {
      jQuery('#err_' + question_index).html('');
    }
  });

  return return_flag;
}
jQuery(document).ready(function () {



  jQuery(".q-rbtn").change(function () {
    updateScore(jQuery(this).val(), jQuery(this).attr('rel'));
    var question_index = jQuery(this).attr('rel');
    if (jQuery(this).val() == "") {
      jQuery('#err_' + question_index).html('Please select an answer for this question');
    } else {
      jQuery('#err_' + question_index).html('');
    }
  });







  jQuery('.btn-pdf-submit').click(function () {
    jQuery('#action_type').val('pdf');
    validate_questions();
    if (validate_questions()) {
      if (jQuery('#total_score').val() != "") {
        jQuery('#hidden-save').click();
      } else {
        alert('Please select options for all the questions.');
        return false;
      }
    }
  });


//=============================part-4   switch_button====================			                                                                  
  jQuery('#switch_button_on').click(function () {
    jQuery('#switch_button').css('background-image', 'url("../sites/gastrocentral_com_au/themes/gastrocentral/img/switch_button1.png")');
    if (jQuery('#mayoCalcualtor .gryBox_switch').css('opacity') == 0 && jQuery('#question_4_score').val() == -1) {
      jQuery('#question_4_score').val('');
    }

    jQuery('#mayoCalcualtor .gryBox_switch').css('opacity', '1');

  });
  jQuery('#switch_button_off').click(function () {
    jQuery('#switch_button').css('background-image', 'url("../sites/gastrocentral_com_au/themes/gastrocentral/img/switch_button2.png")');
    jQuery('#mayoCalcualtor .gryBox_switch').css('opacity', '0');
    for (var i = 0; i < 4; i++) {
      jQuery('#mayoCalcualtor .gryBox_switch input').eq(i).attr("checked", false);
    }
    jQuery('#question_4_score').val('-1');
    var q_score_1 = jQuery('#question_1_score').val();
    var q_score_2 = jQuery('#question_2_score').val();
    var q_score_3 = jQuery('#question_3_score').val();
    var score_1 = isNaN(parseInt(q_score_1)) ? 0 : parseInt(q_score_1);
    var score_2 = isNaN(parseInt(q_score_2)) ? 0 : parseInt(q_score_2);
    var score_3 = isNaN(parseInt(q_score_3)) ? 0 : parseInt(q_score_3);
    var sum = score_1 + score_2 + score_3;
    if (sum == 0 && isNaN(parseInt(q_score_1)) && isNaN(parseInt(q_score_2)) && isNaN(parseInt(q_score_3))) {
      jQuery('#total_score').val('');
    } else {
      jQuery('#total_score').val(sum);
    }
  });
//end=============================part-4   switch_button====================			                                                                  


});
dt = new Date();
function updateScore(val, question_index) {


  var question_score_field_id = 'question_' + question_index + '_score';

  jQuery('#' + question_score_field_id).val(val);
  updateTotalScore();
}

function updateTotalScore() {
  var _total_score = 0;
  var q_val = 0;
  jQuery('.question-score').each(function () {
    if (jQuery(this).val() == "" || jQuery(this).val() == -1) {
      q_val = 0;
    } else
      q_val = parseInt(jQuery(this).val());
    _total_score += q_val;
  });

  jQuery('#total_score').val(_total_score);
}

//print
function PrintElem(elem)
{
  Popup(jQuery(elem).html());
}

function Popup(data)
{
  var mywindow = window.open('', 'MAYO SCORE CALCULATOR', 'scrollbars=1,height=850,width=600');
  //headHtml = jQuery('head').html();

  //footerHtml = '<footer>'+jQuery('footer').html()+'</footer>';
  //content = '<html><head><title>MAYO SCORE CALCULATOR '+'</title>'+headHtml+'</head><body>' +data+footerHtml+'</body></html>';
  content = data;

  mywindow.document.write(content);
  mywindow.document.close();
  mywindow.focus();
  mywindow.print();
  mywindow.close();


  return true;
}

function getUpdateHtml() {

  jQuery('#action_type').val('print');
  if (validate_questions()) {
    if (jQuery('#total_score').val() != "") {
      jQuery.ajax({
        url: window.location.toString(),
        data: jQuery('#frm_calc').serialize(),
        type: 'post'
      }).done(function (r) {
        if (r == 'error') {
          window.location = '<?php echo get_site_url(); ?>';
        } else {
          alert(r);
          Popup(r);
        }

      });
    } else {
      alert('Please select answers for all the questions.');
      return false;
    }
  }
}

						